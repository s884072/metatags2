import React, { Component } from 'react';
import queryString from 'query-string'
import Helmet from "react-helmet";
import { postServices } from '../../src/users/postServices'

class Test extends Component {
    constructor(props) {
        super(props);
        this.state = {
            postData: null
        }
    }

    componentDidMount() {

        try {
            const values = queryString.parse(this.props.location.search)
            console.log(values)
            postServices.postPreView(values.postid, this.FromServerHandler.bind(this));
        } catch (e) {
            console.log(e);
        }
    }
    FromServerHandler(postJSON) {
        console.log(postJSON.result[0]);
        this.setState({
            postData: postJSON.result[0]
        })

    }
    FromServerHandler(postJSON) {
        this.setState({
            postData: postJSON.result[0]
        })
    }
    render() {

        if (this.state.postData !== null) {
            console.log(this.state.postData)
            const isReleased = this.state.postData.isReleased;
            const bgImgClasses = "post-preview-bg-img " + (isReleased ? 'blurred' : 'unblurred');
            const length = this.state.postData.postPreviewContent.backgroundImg !== undefined ? this.state.postData.postPreviewContent.backgroundImg.split('.').length - 1 : 0;
            const hasBgImg = length !== 0 ? (this.state.postData.postPreviewContent.backgroundImg.split('.')[length] === 'mp4' || this.state.postData.postPreviewContent.backgroundImg.split('.')[length] === 'mkv' || this.state.postData.postPreviewContent.backgroundImg.split('.')[length] === 'avi') ? false : true : false;
            const hasBgVideo = length !== 0 ? (this.state.postData.postPreviewContent.backgroundImg.split('.')[length] === 'mp4' || this.state.postData.postPreviewContent.backgroundImg.split('.')[length] === 'mkv' || this.state.postData.postPreviewContent.backgroundImg.split('.')[length] === 'avi') ? true : false : false;
            const body = this.state.postData.postPreviewContent.previewText !== "" ? this.state.postData.postPreviewContent.previewText : "...";
            var htmlObject = document.createElement('div');
            htmlObject.innerHTML = body;

            return (
                <div>
                    <Helmet>
                            <meta property="og:title" content={this.state.postData.postPreviewContent.previewTitle} />

                            <meta property="og:description" content={this.state.postData.postPreviewContent.previewTitle} />
                            <meta property="og:image" content={"http://default.futugard.com:3000/posts/postPreView/" + this.state.postData.thumbnailImg} />

                            <meta name="twitter:title" content={this.state.postData.postPreviewContent.previewTitle} />
                            <meta name="twitter:description" content=" Offering tour packages for individuals or groups." />
                            <meta name="twitter:image" content=" http://euro-travel-example.com/thumbnail.jpg" />
                            <meta name="twitter:card" content="summary_large_image"></meta>

                            <meta property="og:title" content="European Travel Destinations" />
                            <meta property="og:description" content="Offering tour packages for individuals or groups." />
                            <meta property="og:image" content="http://euro-travel-example.com/thumbnail.jpg" />
                            <meta property="og:url" content="http://euro-travel-example.com/index.htm" />
                            <meta name="twitter:card" content="summary_large_image" />

                            <meta property="fb:app_id" content="your_app_id" />
                            <meta name="twitter:site" content="@website-username" />
                            <meta property="og:site_name" content="European Travel, Inc." />
                            <meta name="twitter:image:alt" content="Alt text for image"></meta>
                            <title>{this.state.postData.postPreviewContent.previewTitle}</title>

                            <meta name="viewport" content="width=device-width, initial-scale=1" />


                            <meta name="msvalidate.01" content="15B2F9DC1A9D64AEB0134F21A3D8A683" />


                            <meta name="fontiran.com:license" content="THJBT" />
                            <meta name="robots" content="index, follow" />


                            <meta name="description" content={this.state.postData.postPreviewContent.previewTitle} />


                            <meta name="msapplication-TileColor" content="#ffffff" />
                            <meta name="msapplication-TileImage" content="/ms-icon-144x144.png" />
                            <meta name="theme-color" content="#fb3449" />
                            <meta name="msapplication-navbutton-color" content="#fb3449" />
                            <meta name="apple-mobile-web-app-status-bar-style" content="#fb3449" />

                        </Helmet>
                    {/* //------------------------- Header -------------------- */}
                    <div style={{ height: 60, backgroundColor: '#23170B', justifyItems: 'center' }}>
                        <div style={{ width: '60%', marginLeft: '20%', marginRight: '20%', height: '100%', alignContent: 'flex-end' }}>
                            <div style={{ float: 'right' }}>
                                <button
                                    style={{ marginRight: 20, backgroundColor: "#23170B", borderColor: '#00897b', borderWidth: 2, borderRadius: 10, marginTop: 10, height: 35, width: 70 }}>
                                    <a href='http://www.google.com' style={{ color: '#00897b', height: 50, width: 80, }}>Login</a>
                                </button>
                                <button
                                    style={{ backgroundColor: "#23170B", borderColor: '#ff5e3a', borderWidth: 2, borderRadius: 10, marginTop: 10, height: 35, width: 70 }}>
                                    <a href='http://www.google.com' style={{ color: '#ff5e3a', height: 50, width: 80, }}>Sing Up</a>
                                </button>

                            </div>
                        </div>
                    </div>
                    {/* ----------------------------------- body ------------------------------- */}
                    <div style={{ width: '60%', marginLeft: '20%', marginRight: '20%', height: '100%' }}>
                        <div style={{width:'100%',height:'max-content',textAlign:'center'}}>
                        <img src='https://static.farakav.com/files/pictures/thumb/01432683.jpg' style={{ width: '80%', marginLeft: '10%', marginRight: '10%', marginTop: 20, borderRadius: 20, boxShadow: '0px 1px 6px 1px #888' }} />
                        </div>

                        <div style={{ width: '80%', marginLeft: '10%', marginRight: '10%', marginTop: 20, }}>

                            <h3>
                                {this.state.postData.postPreviewContent.previewTitle}
                            </h3>
                            <span>Create At : {this.state.postData.createAt}</span>
                            <div style={{borderWidth:.5,backgroundColor:'#666',height:2,marginTop:5,marginBottom:10}}></div>



                            <div style={{marginLeft:10}}>
                            <img src='https://image.flaticon.com/icons/svg/126/126473.svg' style={{width:20,height:20}}/>
                            <span style={{marginRight:15,marginLeft:5,marginBottom:2}}>50</span>
                            <img src='https://image.flaticon.com/icons/svg/1380/1380338.svg' style={{width:20,height:20}}/>
                            <span style={{marginRight:15,marginLeft:5,marginBottom:2}}>50</span>
                            <img src='https://image.flaticon.com/icons/svg/126/126473.svg' style={{width:20,height:20}}/>
                            <span style={{marginRight:15,marginLeft:5,marginBottom:2}}>50</span>
                            </div>



                            <div style={{borderWidth:.5,backgroundColor:'#666',height:2,marginTop:10}}></div>

                            <div style={{marginBottom:20}}>
                                <p>{htmlObject.firstChild.textContent}</p>
                            </div>
                        </div>


                    </div>









                    <div>
                        

                        <div className="discover-page">

                            <div className="discover-page-wrapper" >
                                <div className="post-preview-item" style={{ textAlign: 'center', marginTop: '100px', marginBottom: '20px' }}>
                                    {hasBgImg &&

                                        <img alt="" src={"http://default.futugard.com:3000/posts/postPreView/" + this.state.postData.thumbnailImg} className={bgImgClasses} />
                                    }
                                    {hasBgVideo &&
                                        <div onMouseOver={this.onPlay} onMouseLeave={this.onStop}>
                                            <ReactPlayer style={{ transition: 'none !important', height: '84% !important' }}
                                                ref={this.ref}
                                                youtubeConfig={{ playerVars: { showinfo: 1 } }}
                                                url={appConstants.getFetchUrl() + '/' + this.state.postData.postPreviewContent.backgroundImg}
                                                pip={this.state.pip}
                                                playing={this.state.playing}

                                                controls={this.state.controls}
                                                light={this.state.light}
                                                loop={this.state.loop}
                                                playbackRate={this.state.playbackRate}
                                                volume={this.state.volume}
                                                muted={this.state.muted}
                                                height='70%'
                                                width='100%'




                                            />
                                        </div>

                                    }
                                </div>
                                

                            </div>

                        </div>



                    </div>
                </div>
            )
        }
        else {
            return <div></div>
        }
    }
}

export default Test;




