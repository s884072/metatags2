const postServices = {
    postPreView: function (postId, doneCallback) {
        fetch("http://default.futugard.com:3000/posts/postPreView/" + postId, {
        // fetch("http://192.168.1.95:3000/posts/postPreView/" + postId, {
            method: 'POST',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
            }
        })
            .then(function (response) {
                if (response.ok) {
                    return response.json();
                }
                else {
                    return [];
                }
            }).then(function (postsJSON) {
                doneCallback(postsJSON);
            });




    }
}
export { postServices };