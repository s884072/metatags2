import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from '../home/Home';

export default function Layout() {
    return (
        <div>
            <Switch>
                <Route path="/" exact component={Home}></Route>
              
            </Switch>
        </div>
    );
}